module whatsapp_stt

go 1.21.1

require (
	github.com/asticode/go-astiav v0.12.0
	github.com/ggerganov/whisper.cpp/bindings/go v0.0.0-20231004090025-91c0b23384fc
	github.com/krig/go-sox v0.0.0-20180617124112-7d2f8ae31981
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/mdp/qrterminal/v3 v3.1.1
	github.com/pelletier/go-toml/v2 v2.1.0
	go.mau.fi/whatsmeow v0.0.0-20230929093856-69d5ba6fa3e3
	google.golang.org/protobuf v1.31.0
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	go.mau.fi/libsignal v0.1.0 // indirect
	go.mau.fi/util v0.1.0 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	rsc.io/qr v0.2.0 // indirect
)
