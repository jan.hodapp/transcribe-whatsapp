package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/ggerganov/whisper.cpp/bindings/go/pkg/whisper"
	"github.com/krig/go-sox"
	_ "github.com/mattn/go-sqlite3"
	"github.com/mdp/qrterminal/v3"
	"github.com/pelletier/go-toml/v2"
	"go.mau.fi/whatsmeow"
	waProto "go.mau.fi/whatsmeow/binary/proto"
	"go.mau.fi/whatsmeow/store/sqlstore"
	"go.mau.fi/whatsmeow/types"
	"go.mau.fi/whatsmeow/types/events"
	waLog "go.mau.fi/whatsmeow/util/log"
	"google.golang.org/protobuf/proto"
)

var TARGET_GROUP types.JID

type WhatsappClient struct {
	WAClient       *whatsmeow.Client
	eventHandlerID uint32
	whisper        whisper.Model
}

type Config struct {
	SourceGroup string
	SourceUser  string
	TargetGroup string
	ModelPath   string
}

var CONFIG Config

func (client *WhatsappClient) register() {
	client.eventHandlerID = client.WAClient.AddEventHandler(client.myEventHandler)
}

func (client *WhatsappClient) myEventHandler(evt interface{}) {
	switch v := evt.(type) {
	case *events.Message:
		log.Println("Received a message!\nSOURCE_USER_JID: ", v.Info.MessageSource.Sender.User, ", SOURCE_CHAT_JID: ", v.Info.MessageSource.Chat.String())
		if v.Info.MessageSource.Chat.String() == CONFIG.SourceGroup && v.Info.MessageSource.Sender.User == CONFIG.SourceUser {
			log.Println("Matched chat and user", v.Info)
			if v.Info.MediaType == "ptt" {
				audioData, _ := client.WAClient.Download(v.Message.GetAudioMessage())
				result := client.handleAudio(&audioData)
				reply, e := client.WAClient.SendMessage(context.Background(), TARGET_GROUP, &waProto.Message{Conversation: proto.String(result)})
				if e != nil {
					log.Println("Sending message failed: ", e)
				}
				log.Println(reply)
			}
		}
	}
}

func (client *WhatsappClient) handleAudio(audioData *[]byte) string {
	in := sox.OpenMemRead(*audioData)
	if in == nil {
		log.Fatal("Failed to open input file")
	}
	// Close the file before exiting
	defer in.Release()

	//b := make([]byte, 1000000)
	//converted := sox.OpenMemWrite(b, sox.NewSignalInfo(16000, 1, in.Signal().Precision(), in.Signal().Length(), nil), , "wav")
	outLength := uint64(float64(in.Signal().Length()) / in.Signal().Rate() * 16000)

	buf := make([]sox.Sample, in.Signal().Length())
	data := make([]float32, outLength)
	in.Read(buf, uint(in.Signal().Length()))
	for i := uint64(0); i+3 < in.Signal().Length(); i += 3 {
		data[i/3] = float32((sox.SampleToFloat64(buf[i]) + sox.SampleToFloat64(buf[i+1]) + sox.SampleToFloat64(buf[i+2])) / 3)
	}
	//converted := sox.OpenWrite("default", sox.NewSignalInfo(16000, 1, in.Signal().Precision(), outLength, nil), nil, "pulseaudio")
	//converted.Release()
	return client.speechToText(data)
}

func (client *WhatsappClient) speechToText(data []float32) string {
	context, err := client.whisper.NewContext()
	if err != nil {
		panic(err)
	}

	var cb whisper.SegmentCallback
	cb = func(segment whisper.Segment) {
		fmt.Println("Segment", segment.Text)
	}

	context.SetLanguage("de")
	context.ResetTimings()
	if err := context.Process(data, cb, nil); err != nil {
		panic(err)
	}
	//context.PrintTimings()
	var segment (whisper.Segment)
	result := ""
	for err == nil {
		segment, err = context.NextSegment()
		result += segment.Text
	}
	return result
}

func initializeWhatsmeow() *whatsmeow.Client {
	dbLog := waLog.Stdout("Database", "Info", true)
	container, err := sqlstore.New("sqlite3", "file:whatsapp.db?_foreign_keys=on", dbLog)
	if err != nil {
		panic(err)
	}

	// If you want multiple sessions, remember their JIDs and use .GetDevice(jid) or .GetAllDevices() instead.
	deviceStore, err := container.GetFirstDevice()
	if err != nil {
		panic(err)
	}
	client := whatsmeow.NewClient(deviceStore, nil)
	client.Disconnect()
	if client.Store.ID == nil {
		// No ID stored, new login
		qrChan, _ := client.GetQRChannel(context.Background())
		err = client.Connect()
		if err != nil {
			panic(err)
		}
		for evt := range qrChan {
			if evt.Event == "code" {
				qrterminal.GenerateHalfBlock(evt.Code, qrterminal.L, os.Stdout)
				fmt.Println("QR code:", evt.Code)
			} else {
				fmt.Println("Login event:", evt.Event)
			}
		}
	} else {
		err = client.Connect()
		if err != nil {
			panic(err)
		}
	}

	TARGET_GROUP, err = types.ParseJID(CONFIG.TargetGroup)
	if err != nil {
		panic("Parsing JID failed.")
	}
	return client
}

func main() {
	var err error

	//err = toml.Unmarshal([]byte(doc), &cfg)
  reader, err := os.Open("./config.toml")
	if err != nil {
		panic(err)
	}
  decoder := toml.NewDecoder(reader)
	if err != nil {
		panic(err)
	}
  err = decoder.Decode(&CONFIG)
	if err != nil {
		panic(err)
	}
	fmt.Println("Model Path:", CONFIG.ModelPath)
	fmt.Println("Source User:", CONFIG.SourceUser)
	fmt.Println("Source Group:", CONFIG.SourceGroup)
	fmt.Println("Target Group:", CONFIG.TargetGroup)


	client := new(WhatsappClient)
	client.whisper, err = whisper.New(CONFIG.ModelPath)
	if err != nil {
		panic(err)
	}
	defer client.whisper.Close()

	if !sox.Init() {
		log.Fatal("Failed to initialize SoX")
	}
	defer sox.Quit()

	//mycli.WAClient = whatsmeow.NewClient(deviceStore, clientLog)
	client.WAClient = initializeWhatsmeow() // Listen to Ctrl+C (you can also do something else that prevents the program from exiting)

	client.register()
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c

}
