default:
  @just --list

run:
  CGO_ENABLED=1 C_INCLUDE_PATH="$(pwd)/thirdparty/whisper.cpp/" LIBRARY_PATH="$(pwd)/thirdparty/whisper.cpp/" go run ./src

test:
  CGO_ENABLED=1 C_INCLUDE_PATH="$(pwd)/thirdparty/whisper.cpp/" LIBRARY_PATH="$(pwd)/thirdparty/whisper.cpp/" go test -v ./src

build_thirdparty:
  @cd thirdparty/whisper.cpp/bindings/go && make whisper

build: 
  CGO_ENABLED=1 C_INCLUDE_PATH="$(pwd)/thirdparty/whisper.cpp/" LIBRARY_PATH="$(pwd)/thirdparty/whisper.cpp/" go build -o build/transcribe_whatsapp src/main.go
